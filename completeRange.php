<?php

class CompleteRange{
    public function completeRange($array){
        //$array = array(21,25,25,26,26,30);
        $arrayResult = array();
        $aux = 0;
        for($i=min($array);$i<=max($array);$i++){
            $arrayResult[$aux] = $i;
            $aux++;
        }
        return $arrayResult;
    }
}