<?php

class ClearPar{
    public function clearPar(){
        $data = Request::all();
        $word = $data['word'];
        $numPrtns = 0;
        $i = 0;
        while(strlen($word)>0){
            $word = ltrim($word,")");
            $i++;
            if(strlen($word)==1){
                break;
            }else{
                if ($i==2 and $word[1]==")"){
                    $numPrtns++;
                    $i=0;
                    $word = substr($word,2);
                }
                if($i==2 and $word[1]=="("){
                    $word = substr($word,1);
                    $i=0;
                }
            }
        }
        $arrayResult = str_repeat("()",$numPrtns);
        return $arrayResult;
    }
}