<?php

class ChangeString{
    public function build($word){
        $word = utf8_decode($word);
        $result = "";
        for($i=0;$i<strlen($word);$i++){
            if (ctype_alpha($word[$i])){
                if ($word[$i]=="n"){
                    $result = $result.htmlentities("ñ");
                }elseif ($word[$i]=="N"){
                    $result = $result.htmlentities("Ñ");
                }else{
                    $aux = $word[$i];
                    $agregar = ++$aux;
                    if (strlen($agregar) > 1) {
                        $result = $result.$agregar[0];
                    }else{
                        $result = $result.$agregar;
                    }
                }
            }elseif ($word[$i]==(utf8_decode("ñ"))){
                $result = $result."o";
            }elseif ($word[$i]==(utf8_decode("Ñ"))){
                $result = $result."O";
            }else{
                $result = $result.$word[$i];
            }
        }
        return $result;
    }
}