<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';


$app = new \Slim\App;

$app->get('/', function () {
    $data = file_get_contents("data/employees.json");
    $employees = json_decode($data, true);
    echo '<form action="" method="GET">
            Search: <input type="text" name="term" placeholder="email..."/>
            <input type="submit" value="Submit" />
        </form>';
    echo "<table>";
    echo "<thead><tr><th>Name</th><th>Email</th><th>Position</th><th>Salary</th><th>Detail</th></tr></thead>";
    echo "<tbody>";
    if (!empty($_REQUEST['term'])) {

        foreach ($employees as $employee) {
            if ($employee["email"] == $_REQUEST['term']) {
                echo "<tr><td>" . $employee["name"] . "</td>";
                echo "<td>" . $employee["email"] . "</td>";
                echo "<td>" . $employee["position"] . "</td>";
                echo "<td>" . $employee["salary"] . "</td>";
                //echo "<td><a href=\"detailEmployee?id=" . $employee["id"] . "\">detalle</a></td></tr>";
                echo "<td><a href=\"detailEmployee\">detalle</a></td></tr>";
            }
        }
    }else{
        foreach ($employees as $employee) {
            echo "<tr><td>" . $employee["name"] . "</td>";
            echo "<td>" . $employee["email"] . "</td>";
            echo "<td>" . $employee["position"] . "</td>";
            echo "<td>" . $employee["salary"] . "</td>";
            echo "<td><a href=\"index.php/detailEmployee/" . $employee["id"] . "\">detalle</a></td></tr>";

        }
    }
    echo "</tbody><table>";
});

$app->get('/detailEmployee/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $data = file_get_contents("data/employees.json");
    $employees = json_decode($data, true);
    echo '<form action="/problema" method="POST">            
            <input type="submit" value="HOME" />
        </form>';
    foreach ($employees as $employee) {
        if ($employee["id"] == $id) {
            echo "<h1>Name: ".$employee["name"]."</h1>";
            echo "<h3>Email: ".$employee["email"]."</h3>";
            echo "<h3>Phone: ".$employee["phone"]."</h3>";
            echo "<h3>Address: ".$employee["address"]."</h3>";
            echo "<h3>Position: ".$employee["position"]."</h3>";
            echo "<h3>Salary: ".$employee["salary"]."</h3>";
            echo "<h3>Skills:</h3>";
            foreach ($employee["skills"] as $skill) {
                echo "<h4>&nbsp;&nbsp;&nbsp;- Skill: ".$skill["skill"]."</h4>";
            }

        }
    }
});

$app->post('/api_employees', function (Request $request, Response $response) use ($app) {
    try {
        $data = file_get_contents("data/employees.json");
        $employees = json_decode($data, true);

        $min = $request->getAttribute('min');
        $max = $request->getAttribute('max');

        $response = $response->withHeader('Content-type', 'application/xml');

        $xml = new SimpleXMLElement('<root/>');
        foreach ($employees as $r) {
            if (str_replace(",","",substr($r['salary'],1))>$min and str_replace(",","",substr($r['salary'],1))<$max) {
                $item = $xml->addChild('item');
                $item->addChild('id', $r['id']);
                $item->addChild('name', $r['name']);
                $item->addChild('email', $r['email']);
                $item->addChild('position', $r['position']);
            }
        }
        echo $xml->asXML();

    } catch (Exception $e) {
        $app->$response->status(400);
        $app->$response->header('X-Status-Reason', $e->getMessage());
    }
});
$app->run();